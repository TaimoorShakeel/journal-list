
PYTHON := python3

SOURCEDIR := .
SOURCES := $(shell find $(SOURCEDIR) -name '*.yaml')


all:
	$(PYTHON) convert.py $(SOURCES)

sort:
	$(PYTHON) sort.py -r files $(SOURCES)

.PHONY: all sort